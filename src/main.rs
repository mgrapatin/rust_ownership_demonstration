fn main() {
    //String Initialisierung
    //auch durch String::from() mgl.
    let bachelor = "thesis".to_string();
    print_string(bachelor);
    print_string(bachelor);
}

fn print_string(s: String) {
    println!("{}", s);
}
